"set laststatus=2

"let g:airline_theme = 'molokai'
"let g:airline#extensions#branch#enabled = 1
"
"if !exists('g:airline_symbols')
"  let g:airline_symbols = {}
"endif
"
"" powerline symbols
"let g:airline_left_sep = ''
"let g:airline_left_alt_sep = ''
"let g:airline_right_sep = ''
"let g:airline_right_alt_sep = ''
"let g:airline_symbols.branch = ''
"let g:airline_symbols.readonly = ''
"let g:airline_symbols.linenr = ''
"
"let g:airline#extensions#syntastic#enabled = 1