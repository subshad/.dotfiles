"SYNTAX
" load file type plugins + indentation
filetype plugin indent on
syntax on

set omnifunc=syntaxcomplete#Complete

autocmd BufNewFile,BufRead \*.{md,mdwn,mkd,mkdn,mark\*} set filetype=markdown
let g:markdown_fenced_languages = ['coffee', 'css', 'erb=eruby', 'javascript', 'js=javascript', 'json=javascript', 'ruby', 'sass', 'xml', 'html']

"ENCODING
" Use UTF-8 without BOM
set encoding=utf-8 nobomb
set fileencoding=utf-8
set fileencodings=utf-8

" format files correctly
set binary
set noeol
set list
set listchars=tab:›\ ,trail:•,extends:#,nbsp:. " Highlight problematic whitespace

"HIGHLIGHTING
set showmatch                   " Show matching brackets/parenthesis

"COMMAND WRAPPING
set whichwrap=b,s,h,l,<,>,[,]   " Backspace and cursor keys wrap too

"HTMLINDENT
let g:html_indent_tags = 'li\|p'


"FOLDING
set foldenable
set foldlevelstart=0
set foldnestmax=10
set foldmethod=indent

"BACKUP / SWAP
set nobackup
set nowritebackup
set noswapfile
set history=1000

"CLIPBOARD
set clipboard=unnamed

"SPLITS
" Always use vertical diffs
set diffopt+=vertical
" Open new split panes to right and bottom, which feels more natural
set splitbelow
set splitright

"SPELLING
set spellfile=$HOME/.vim-spell-en.utf-8.add

"SEARCHING
" highlight matches
set hlsearch
" incremental searching
set incsearch
" searches are case insensitive...
set ignorecase
" ... unless they contain at least one capital letter
set smartcase
" match everywhere by default
set gdefault

"BACKSPACE
set backspace=indent,eol,start  " Backspace for dummies

"INDENTATION
set autoindent

"TABS
" a tab is two spaces
set tabstop=2
set shiftround
set shiftwidth=2
set softtabstop=2
" use spaces, not tabs (optional)
set expandtab

"PASTE
" pastetoggle (sane indentation on pastes)
set pastetoggle=<F12>

"WIDTH
" Make it obvious where 80 characters is
set colorcolumn=81
set wrap

"LINES
set cursorline
set linespace=0                 " No extra spaces between rows
set rnu
set number
set numberwidth=5