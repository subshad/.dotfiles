# keybinding
set -o vi
#bindkey "^B" beginning-of-line
#bindkey "^E" end-of-line
##bindkey "^R" history-incremental-search-backward
##bindkey "^P" history-search-backward
#bindkey "^Y" accept-and-hold
#bindkey "^N" insert-last-word
#bindkey -s "^T" "^[Isudo ^[A" # "t" for "toughguy"

# aliases
source $HOME/.aliases

# fzf
source $HOME/.fzf.bash

# rbenv
export PATH="$HOME/.rbenv/bin:$PATH"
export PATH="$HOME/.rbenv/plugins/ruby-build/bin:$PATH"
eval "$(rbenv init - zsh --no-rehash)"

export PATH="$HOME/.bin:$PATH"

# environment variables
if [[ -e /usr/share/terminfo/x/xterm-256color ]]; then
  export TERM='screen-256color'
else
  export TERM='xterm-color'
fi

export VISUAL=vim
export EDITOR=$VISUAL

export LANG='en_AU.UTF-8'
export LESS='-F -g -i -M -R -S -w -X -z-4'
export CLICOLOR=1
export GOPATH=$HOME/development/apps/go

# paths
export PATH="$HOME/.bin:$PATH"
export PATH="/usr/local/bin:$PATH"

### Added by the Heroku Toolbelt
export PATH="/usr/local/heroku/bin:$PATH"

export NVM_DIR=~/.nvm
source $(brew --prefix nvm)/nvm.sh


if [ -f $(brew --prefix)/etc/bash_completion ]; then
  . $(brew --prefix)/etc/bash_completion
fi

#Prompt
source /usr/local/etc/bash_completion.d/git-prompt.sh

get_dir() {
    printf "%s" $(pwd | sed "s:$HOME:~:")
}

get_sha() {
    git rev-parse --short HEAD 2>/dev/null
}

GIT_PS1_SHOWDIRTYSTATE=1
GIT_PS1_SHOWSTASHSTATE=1
GIT_PS1_SHOWUNTRACKEDFILES=1
GIT_PS1_SHOWCOLORHINTS=1
GIT_PS1_DESCRIBE_STYLE="branch"
GIT_PS1_SHOWUPSTREAM="auto git"

PROMPT_COMMAND='__git_ps1 "\u \W" "\\\$ " " [%s $(get_sha)] "'
[ -f ~/.fzf.bash ] && source ~/.fzf.bash
