if has('vim_starting')
  " choose no compatibility with legacy vi
  set nocompatible
  " set modelevel 1
  set modelines=1
endif
