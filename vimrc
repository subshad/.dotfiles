let s:vim_config = '~/.dotfiles/vim/config'

for f in split(globpath(s:vim_config,'**/*.vim'),'\n')
  exec 'source '.f
endfor
