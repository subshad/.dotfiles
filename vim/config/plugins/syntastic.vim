" configure syntastic syntax checking to check on open as well as save
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list  = 1
let g:syntastic_auto_loc_list             = 2
let g:syntastic_check_on_open             = 1
let g:syntastic_check_on_wq               = 1
let g:syntastic_aggregate_errors          = 1
let g:syntastic_loc_list_height           = 5

let g:syntastic_error_symbol              = '✗✗'
let g:syntastic_warning_symbol            = '⚠⚠'
let g:syntastic_style_error_symbol        = '✗'
let g:syntastic_style_warning_symbol      = '⚠'

let g:syntastic_html_tidy_ignore_errors   = [" proprietary attribute \"ng-"]
let g:syntastic_sh_checkers               = ['shellcheck', 'checkbashisms', 'sh']
let g:syntastic_sh_checkbashisms_args     = '-x'
let g:syntastic_ruby_checkers             = ['mri', 'jruby', 'rubocop']
let g:syntastic_ruby_rubocop_args         = '--display-cop-names'
let g:syntastic_scss_checkers             = ['sass']
let g:syntastic_sass_checkers             = ['sass']
let g:syntastic_xml_checkers              = ['xmllint']
let g:syntastic_xslt_checkers             = ['xmllint']
let g:syntastic_yaml_checkers             = ['jsyaml']
let g:syntastic_javascript_checkers       = ['eslint']
